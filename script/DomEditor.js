domeEditorv4 = (function (target) {
	this.target = target;

		//liste des éditeurs
	listEditeur = Array();


		
	htmlHead = '<div class="accordion" id="accordionExemple">';

		//console.log(htmlHead);
		//Modèle du code pour afficher un éditeur
	htmlEditeur = '<div >\
	<div class="" id="headingOne">\
	<h2 class="mb-0">\
	<button class="btn  btn-block editeur " type="button" data-toggle="collapse" data-value="{edt.data}" data-target="#{edt.datatarget}" aria-expanded="false" aria-controls="{edt.datatarget}">\
	{edt.name}\
	</button>\
	</h2>\
	</div>\
	<div id="{edt.datatarget}" class="pl-5  categorie collapse" aria-labelledby="headingOne" data-parent="#accordionExemple">\
	<ul>\
	{edt.badge}\
	</ul>\
	</div>\
	</div>';



		//Modèle pour l'affichage d'un badge
	htmlBadge = '<li><button class="btn btn-block badgebtn ajax" data-value="{img.data}" type="button">{img.name}</button></li>';


	htmlFooter = '</div>';

	/**
	* Récupère un éditeur en fontion de son id
	*/
	getEditeur = (function (argument) {
		return this.listEditeur[argument];
	}).bind(this);

	/**
	* Ajoute un éditeur à la liste des éditeurs
	* Entrées : id : l'id de l'éditeur à ajouter
	*			name : le nom de l'éditeur
	*			data : les données associées à l'éditeur
	*/
	addEditeur = (function(id, name, data) {
		this.listEditeur.push(
		{
			nameEditeur : name,
			idEditeur : id,
			dataEditeur : data,
			listBadge : Array()
		});

	}).bind(this);

	getEditeur = (function(id) {
		return listEditeur[id];
	}).bind(this);



	badge = {
		nameBadge : "",
		imgBadge : "",
		dataBadge : ""
	};

	/**
	* Ajoute un badge à la liste des badges pour la catégorie et l'éditeur correspondant
	* Entrées : idEditeur : l'id de l'éditeur du badge
	*			id : l'id du badge à ajouter
	*			name : le nom du badge
	*			data : les données associées au badge
	*/
	addBadge = (function (idEditeur, id, name, data) {
		this.listEditeur[idEditeur].listBadge.push(
		{
			idBadge : id,
			nameBadge : name,
			dataBadge : data
		});
	}).bind(this);

	/**
	* Affiche les éditeur, leurs catégories et les badges correspondants
	*/
	display = (function() {
	    

		//stock le html pour les éditeurs

		editeurDone = htmlHead;
		for (var i = 0; i <  this.listEditeur.length; i++) {

			//récupère un éditeur dans la liste des éditeurs
			editeur = this.listEditeur[i];

				//récupère la liste des badges de cet editeur
			listBadge = editeur.listBadge;
			//stock le code html correspondant aux badges de l'éditeur
			htmlbadgeDone = "";

				for (var k = 0; k < listBadge.length; k ++) {

					//récupère un badge
					badge = listBadge[k];

					//Génère le html pour le badge
					copyBadgeTML = this.htmlBadge;
					htmlbadgeDone += copyBadgeTML.replace(/\{img\.data\}/gi, badge.dataBadge)
					.replace(/\{img\.name\}/gi, badge.nameBadge);

				}

			//génére le html pour l'éditeur
			copyEditeurHTML = this.htmlEditeur;
			editeurDone += copyEditeurHTML.replace(/\{edt\.datatarget\}/gi, 'edit'+ editeur.idEditeur)
			.replace(/\{edt\.name\}/gi, editeur.nameEditeur)
			.replace(/\{edt\.data\}/gi, editeur.dataEditeur)
			.replace(/\{edt\.badge\}/gi, htmlbadgeDone);
		}
					editeurDone += htmlFooter;

		//insert le html dans le document
			if (/\.[A-z0-9]+/.test(this.target)) {
				this.target =
				document.getElementByClass(this.target.replace('.', '')).foreach(function (item, index) {
					item.innerHTML  = editeurDone;
				});
			}
			else if(/\#[A-z0-9]+/.test(this.target)) {
				document.getElementById(this.target.replace('#', '')).innerHTML  = editeurDone;
			}

		}).bind(this);


	return this;
});
