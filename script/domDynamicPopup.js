/**
 * Générateur de popup à partir d'une chaine de caractères contenant une liste de badges
 */
popUpGenerator = (function () {

	//Blocs html pour générer la popup
	htmlHead = '<div class="text-center">';

	htmlUser = '<table class="col-12 text-center">\
		<tr>\
			<td>\
				{user.id}\
			</td>\
		</tr>\
	</table>';

	htmlDivEditeurBadge = '<div style="height: 250px; overflow-y: scroll; overflow-x: hidden;">\
	<div class="accordion" id="accordionPopUp">';

	htmlEditeurBadgeStart = '<div class="" id="headingOne">\
	<button class="btn btn-block editeur" type="button" data-toggle="collapse" data-target="#{edt.datatarget}" aria-expanded="false" aria-controls="{edt.datatarget}">\
    {edt.name}</button>\
	<div class="collapse" id="{edt.datatarget}" aria-labelledby="headingOne" data-parent="#accordionPopUp">\
	<table class="col-12 text-center">';

	htmlEditeurBadgeElement = '<tr>\
			<td>\
				<img src="{badge.img}" class="image" width="50">\
			</td>\
			<td>\
				{badge.name}\
			</td>\
		</tr>';

	htmlEditeurBadgeEnd = '</table>\
	</div>\
	</div>';

	htmlFooter = '</div>\
	</div>\
	</div>';

	//Fonction qui retourne la popup au format html
	getPopUpString = (function(arrayString) {
		//List de badges qui contiendra les badges ordonnés par éditeur
		listBadges = Array();

		//Découpage de la chaine de caractères pour remplir la liste de badges
		for(var i=1; i < arrayString.length; i++){
			var splittedString = arrayString[i].split(';');
			this.listBadges.push(
			{
				nameBadge : splittedString[2],
				imgBadge : splittedString[1],
				editeur : splittedString[0]
			});
		}

		//Tri de la liste de badges en fonction des éditeurs
		this.listBadges = this.listBadges.sort(compare);

		//Génération du html
		popUpDone = htmlHead;

		copyUserTML = this.htmlUser;
		popUpDone += copyUserTML.replace(/\{user\.id\}/gi,arrayString[0]);
		popUpDone += this.htmlDivEditeurBadge;

		var currentEditeur = "";
		var call = 0;
		
		//Pour chaque badge on génère le html correspondant
		var countEditeur = 0;
		for (var i = 0; i <  this.listBadges.length; i++) {
			
			if(this.listBadges[i].editeur != currentEditeur){
				if(i != 0){
					popUpDone += this.htmlEditeurBadgeEnd;
				}
				currentEditeur = this.listBadges[i].editeur;
				copyBadgeStartTML = this.htmlEditeurBadgeStart;
				countEditeur = countEditeur+1;
				popUpDone += copyBadgeStartTML.replace(/\{edt\.name\}/gi, this.listBadges[i].editeur).replace(/\{edt\.datatarget\}/gi, "edit"+countEditeur);
			}

			copyBadgeElementTML = this.htmlEditeurBadgeElement;
			popUpDone += copyBadgeElementTML.replace(/\{badge\.img\}/gi, this.listBadges[i].imgBadge).replace(/\{badge\.name\}/gi, this.listBadges[i].nameBadge);

		}

		popUpDone += htmlFooter;
		return popUpDone;

	}).bind(this);
	
	//Fonction de tri pour classer la liste de badges en fonction des éditeurs
	function compare( a, b ) {
	  if ( a.editeur < b.editeur ){
		return -1;
	  }
	  if ( a.editeur > b.editeur ){
		return 1;
	  }
	  return 0;
	}

	return this;
});
