<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8" />
	<title>A MAP TEST</title>

	<script type="text/javascript" src="ajax/jquery-3.3.1.min.js"></script>

	<link rel="stylesheet" href="script/leaflet/leaflet.css" />
	<script src="script/leaflet/leaflet.js"></script>
	<script src="script/sidebar_leaflet/js/leaflet-sidebar.min.js"></script>
	<link rel="stylesheet" href="script/sidebar_leaflet/css/leaflet-sidebar.min.css" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">

	<link rel="stylesheet" href="script/marker_leaflet/dist/MarkerCluster.css" />
	<link rel="stylesheet" href="script/marker_leaflet/dist/MarkerCluster.Default.css" />
	<script src="script/marker_leaflet/dist/leaflet.markercluster-src.js"></script>

	<script src="script/domEditor.js"></script>
	<script src="script/domDynamicPopup.js"></script>
	<script src="ajax/ajax.js"></script>
</head>
<body>

<div class="container">
	<div id="mySidenav" class="sidenav">
		
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div id="menu">
		</div>
	</div>

	<div class="row">
		<div class="map_div">
			<div id="mapid" class="sidebar-map"></div>
		</div>
	</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="bootstrap/js/bootstrap.min.js" ></script>
	<script>

	mymap = L.map('mapid').setView([47.3904,0.6894], 13);

	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
		maxZoom: 18,
	}).addTo(mymap);

	mymap.zoomControl.setPosition('topright');

	var ourCustomControl = L.Control.extend({
	  options: {
		position: 'topleft'
		//control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
	  },

	  onAdd: function (map) {
		/*var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');

		container.style.backgroundColor = 'white';
		container.style.width = '30px';
		container.style.height = '30px';
		container.style.text = "Filtres";*/

		var container = L.DomUtil.create('input', 'leaflet-bar leaflet-control leaflet-control-custom');
		container.style.backgroundColor = 'white';
		container.style.fontSize="15px";
		container.type="button";
		container.value="Filtres";

		container.onclick = function(){
			openNav();
		}
		return container;
	  },
	});


	mymap.addControl(new ourCustomControl());
		//test fichier GeoJSON
		fetch('./regions.json').then(response => {
			return response.json();
		}).then(data => {
			L.geoJson(data,
				{style: {
					fillColor: '#468499',
					color: 'grey',
					fillOpacity: 0.2
				}}).addTo(mymap);
		}).catch(err => {
			// Do something for an error here
		});

			/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
		function openNav() {
		  document.getElementById("mySidenav").style.width = "50%";
		}

		/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
		function closeNav() {
		  document.getElementById("mySidenav").style.width = "0";
		}
	</script>

</body>

</html>
