// VARIABLES GLOBALES
var urlRecupFiltres = "https://eos.discendum.com/app/obpv1/location/explore/filters";
var urlRecupBadges = "https://eos.discendum.com/app/obpv1/location/explore/badges";
var markersCluster = L.markerClusterGroup();

// ce tableau permettra de stocker tous les badges obtenus pour chaque utilisateur
// (on l'utilise dans l'affichage des popup en cliquant sur les badges)
var tableauUsersBadges;

var dom = domeEditorv4("#menu");
//////////////////////////////////////////////////////////////
// fonction qui permet d'afficher les points sur la map
// data : tableau contenant des données JSON
//////////////////////////////////////////////////////////////
function afficherDonnees(data){
  var domPopup;
  domPopup = popUpGenerator();

  var points = [];
  //pour chaque badge, on récupère les coordonnées et on les affiche sur la map
  for(badge in data.badges){
    var latlng = [data.badges[badge].lat,data.badges[badge].lng];
    // on parcourt le tableau de badges obtenus pour chaque utilisateur
    // et on vérifie si le user_id du badge en cours est présent dans
    // le tableau. Si oui, on remplit la popup du badge avec tous les badges
    // obtenus pour cet utilisateur
    for(user in tableauUsersBadges){
      if(tableauUsersBadges[user][0] == data.badges[badge].user_id){
          markersCluster.addLayer(L.marker(latlng).bindPopup(L.popup({minWidth:200}).setContent(domPopup.getPopUpString(tableauUsersBadges[user]))));
        break;
      }
    }
  }
  mymap.addLayer(markersCluster);
}

//////////////////////////////////////////////////////////////
// fonction qui permet de récupérer la liste des badges obtenus
// pour chaque utilisateur
// data : tableau contenant des données JSON
//////////////////////////////////////////////////////////////
function recupererBadgesUsers(data){
  var tableauUsersBadges = [];
  var pushed = false;
  for(badge in data.badges){
    for(user in tableauUsersBadges){
      if(tableauUsersBadges[user][0] == data.badges[badge].user_id){
        tableauUsersBadges[user].push(data.badges[badge].issuer_name + " ; " + data.badges[badge].badge_image + " ; " + data.badges[badge].badge_name);
        pushed=true;
        break;
      }
    }
    if(pushed == false){
      var buffer = [];
      buffer.push(data.badges[badge].user_id);
      buffer.push(data.badges[badge].issuer_name + " ; " + data.badges[badge].badge_image + " ; " + data.badges[badge].badge_name);
      tableauUsersBadges.push(buffer);
    }else{
      pushed = false;
    }
  }
  return tableauUsersBadges;
}

jQuery(document).ready(function($) {
  // ce tableau contiendra les filtres (la liste des éditeurs, des badges et les mots-clés)
  var filtres = [];
  // 1ère REQUETE AJAX : permet de récupérer la liste des éditeurs, des badges et les mots-clés associés
  // aux badges
  $.ajax({
    type: "GET",
    dataType: "json",
    url: urlRecupFiltres,
    async: false
  }).done(function(data){
    filtres.push(data);
  });

  // on parcourt la liste des éditeurs et des badges pour les ajouter au DOM
  for(editeurIndex in filtres[0].issuer_name){
    dom.addEditeur(editeurIndex+1,filtres[0].issuer_name[editeurIndex], filtres[0].issuer_name[editeurIndex]);
    for(badgeIndex in filtres[0].badge_name){
      dom.addBadge(editeurIndex, badgeIndex+1, filtres[0].badge_name[badgeIndex], filtres[0].badge_name[badgeIndex]);
    }
  }

  // 2ème REQUETE AJAX pour récupérer la liste de tous les badges
  $.ajax({
    type: "GET",
    dataType: "json",
    url: urlRecupBadges,
    data: {
      "max_lat": 90,
      "max_lng": 180,
      "min_lat": -90,
      "min_lng": -180
    }
  }).done(function(data){
    tableauUsersBadges = recupererBadgesUsers(data);
    //console.log(tableauUsersBadges);
    //console.log(data);
    afficherDonnees(data);
  });

  var nomEditeurSelectionne;
  var nomBadgeSelectionne;
  // Quand on clique sur un éditeur dans la sidenav
  $(".editeur").click(function(){
      console.log("click");
    markersCluster.clearLayers();
    nomEditeurSelectionne = $(this).attr('data-value');
    // 3ème REQUETE AJAX pour récupérer la liste des badges pour l'éditeur sélectionné
    $.ajax({
      type: "GET",
      dataType: "json",
      url: urlRecupBadges,
      data: {
        "max_lat": 90,
        "max_lng": 180,
        "min_lat": -90,
        "min_lng": -180,
        "issuer_name": nomEditeurSelectionne
      }
    }).done(function(data){
      afficherDonnees(data);
      console.log("sucess");
    });
  });

  // Quand on clique sur un badge dans la sidenav
  $(".badgebtn").click(function(){
      console.log("click");
    markersCluster.clearLayers();
    var nomBadgeSelectionne = $(this).attr('data-value');
    // 4ème REQUETE AJAX pour récupérer la liste des badges pour le badge sélectionné pour l'éditeur sélectionné
    $.ajax({
      type: "GET",
      dataType: "json",
      url: urlRecupBadges,
      data: {
        "max_lat": 90,
        "max_lng": 180,
        "min_lat": -90,
        "min_lng": -180,
        "issuer_name": nomEditeurSelectionne,
        "badge_name": nomBadgeSelectionne
      }
    }).done(function(data){
      afficherDonnees(data);
    });
  });

  // on affiche le Dom dans la sidenav
  dom.display();
});
